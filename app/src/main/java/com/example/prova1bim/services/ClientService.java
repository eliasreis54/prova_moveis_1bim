package com.example.prova1bim.services;

import android.os.Build;

import androidx.annotation.RequiresApi;

import com.example.prova1bim.models.Client;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class ClientService {
    private static List<Client> clientList = new ArrayList<>();

    public static void createClient(Client client) {
        clientList.add(client);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public static Client getClientFromEmail(String clientEmail) {
        Predicate<Client> condition = client -> client.getEmail().equals(clientEmail);

        List<Client> client = clientList.stream().filter(condition).collect(Collectors.toList());
        if (client.size() == 0) {
            return null;
        }
        return client.get(0);
    }

    public static List<Client> getClients() {
        return clientList;
    }
}

