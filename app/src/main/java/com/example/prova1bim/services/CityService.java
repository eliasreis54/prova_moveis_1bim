package com.example.prova1bim.services;

import android.os.Build;
import android.widget.ArrayAdapter;

import androidx.annotation.RequiresApi;

import com.example.prova1bim.models.City;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class CityService {
    private static List<City> citiesList = new ArrayList<>();

    public static void createCity(City client) {
        citiesList.add(client);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public static City getCityFromName(String cityName) {
        Predicate<City> condition = city -> city.getName().equals(cityName);

        List<City> cities = citiesList.stream().filter(condition).collect(Collectors.toList());
        if (cities.size() == 0) {
            return null;
        }
        return cities.get(0);
    }

    public static List<City> getAllCities() {
        return citiesList;
    }
}
