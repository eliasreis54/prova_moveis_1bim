package com.example.prova1bim;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.widget.TextView;

import com.example.prova1bim.models.Client;

public class ClientDetailsActivity extends AppCompatActivity {

    TextView clientName;
    TextView clientEmail;
    TextView clientPhone;
    TextView clientCityDetails;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client_details);

        clientName = findViewById(R.id.clientName);
        clientEmail = findViewById(R.id.clientEmail);
        clientPhone = findViewById(R.id.clientPhone);
        clientCityDetails = findViewById(R.id.clientCityInfo);

        try {
            Bundle extras = getIntent().getExtras();
            if (extras != null) {
                Client client = (Client) getIntent().getSerializableExtra("client");

                clientName.setText("Nome: " + client.getName());
                clientEmail.setText("Email: " + client.getEmail());
                clientPhone.setText("Telefone: " + client.getPhone());
                clientCityDetails.setText("Cidade: " + client.getCity() + " com, "
                        + client.getCity().getAge() + " anos de idade e "
                        + client.getCity().getNumberOfInhabitants() + " Habitantes");
            }
        } catch(Exception e) {
            System.out.println(e.getMessage());
        }
    }
}