package com.example.prova1bim;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.prova1bim.models.City;
import com.example.prova1bim.services.CityService;

public class RegisterCityActivity extends AppCompatActivity {
    EditText cityName;
    EditText cityAge;
    EditText cityPopulation;
    Button submitButton;

    private void submitCity() {
        String cityNameValue = cityName.getText().toString();
        int cityAgeValue = Integer.parseInt(cityAge.getText().toString());
        int populationValue = Integer.parseInt(cityPopulation.getText().toString());

        City city = new City(cityNameValue, cityAgeValue, populationValue);

        CityService.createCity(city);

        Toast.makeText(RegisterCityActivity.this,
                "Cidade cadastrado com sucesso", Toast.LENGTH_SHORT).show();

        Intent navigateToHome = new Intent(RegisterCityActivity.this, MainActivity.class);
        startActivity(navigateToHome);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_city);

        cityName = findViewById(R.id.cityName);
        cityAge = findViewById(R.id.cityAge);
        cityPopulation = findViewById(R.id.cityPopulation);
        submitButton = findViewById(R.id.buttonSubmit);

        submitButton.setOnClickListener(v -> submitCity());
    }
}