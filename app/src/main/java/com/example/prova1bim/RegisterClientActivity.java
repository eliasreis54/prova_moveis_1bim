package com.example.prova1bim;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.prova1bim.models.City;
import com.example.prova1bim.models.Client;
import com.example.prova1bim.services.CityService;
import com.example.prova1bim.services.ClientService;

public class RegisterClientActivity extends AppCompatActivity {
    EditText name;
    EditText email;
    EditText phone;
    Spinner citiesList;
    Button submit;

    private void submitClient() {
        String clientName = name.getText().toString();
        String clientEmail = email.getText().toString();
        String clientPhone = phone.getText().toString();
        City city = (City) citiesList.getSelectedItem();

        Client client = new Client(clientName, clientEmail, clientPhone, city);

        ClientService.createClient(client);

        Toast.makeText(RegisterClientActivity.this,
                "Cliente cadastrado com sucesso", Toast.LENGTH_SHORT).show();

        Intent navigateToHome = new Intent(RegisterClientActivity.this, MainActivity.class);
        startActivity(navigateToHome);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_client);

        name = findViewById(R.id.clientName);
        email = findViewById(R.id.clientEmail);
        phone = findViewById(R.id.clientPhone);
        citiesList = findViewById(R.id.city_list);
        submit = findViewById(R.id.submitButtom);

        ArrayAdapter<City> cityAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, CityService.getAllCities());
        citiesList.setAdapter(cityAdapter);

        submit.setOnClickListener(v -> submitClient());
    }
}