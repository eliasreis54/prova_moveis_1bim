package com.example.prova1bim.models;

import java.io.Serializable;

public class City implements Serializable {
    private String name;
    private int age;
    private int numberOfInhabitants;

    public City(String name, int age, int numberOfInhabitants) {
        this.name = name;
        this.age = age;
        this.numberOfInhabitants = numberOfInhabitants;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getNumberOfInhabitants() {
        return numberOfInhabitants;
    }

    public void setNumberOfInhabitants(int numberOfInhabitants) {
        this.numberOfInhabitants = numberOfInhabitants;
    }

    @Override
    public String toString() {
        return name;
    }
}
