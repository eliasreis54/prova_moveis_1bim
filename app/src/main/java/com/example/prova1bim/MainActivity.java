package com.example.prova1bim;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button navigateRegisterCity;
    Button navigateRegisterClient;
    Button navigateRegisterSearchClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        navigateRegisterCity = findViewById(R.id.navigate_to_register_city);
        navigateRegisterClient = findViewById(R.id.navigate_to_register_client);
        navigateRegisterSearchClient = findViewById(R.id.navigate_to_search_client);

        navigateRegisterCity.setOnClickListener(v -> {
            Intent navigateToCity = new Intent(MainActivity.this, RegisterCityActivity.class);
            startActivity(navigateToCity);
        });

        navigateRegisterClient.setOnClickListener(v -> {
            Intent navigateToClient = new Intent(MainActivity.this, RegisterClientActivity.class);
            startActivity(navigateToClient);
        });

        navigateRegisterSearchClient.setOnClickListener(v -> {
            Intent navigateToSearchClient = new Intent(MainActivity.this, ListClientsActivity.class);
            startActivity(navigateToSearchClient);
        });
    }
}