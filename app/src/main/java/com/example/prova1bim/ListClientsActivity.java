package com.example.prova1bim;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.prova1bim.models.Client;
import com.example.prova1bim.services.ClientService;

public class ListClientsActivity extends AppCompatActivity {

    ListView clientList;

    private void navigateToClientDetails(Client client) {
        Intent details = new Intent(ListClientsActivity.this, ClientDetailsActivity.class);
        details.putExtra("client", client);
        startActivity(details);
    }

    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_clients);

        clientList = findViewById(R.id.listClients);

        ArrayAdapter<Client> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, ClientService.getClients());
        clientList.setAdapter(adapter);

        clientList.setOnItemClickListener((parent, view, position, id) -> {
            Client client = (Client) adapter.getItem(position);
            navigateToClientDetails(client);
        });

    }
}